var domain = 'domain'


casper.test.begin('directory 1 (DirectoryIndex)', 1, function suite(test) {
	casper.start(domain + "/test/rewriteSpec1/", function() {
		test.assertTitle("index.html", "rewriteSpec1/ is index.html");
	});
	casper.run(function() {
		test.done();
	});
});

casper.test.begin('directory 2 (DirectoryIndex)', 1, function suite(test) {
	casper.start(domain + "/test/rewriteSpec2/", function() {
		test.assertTitle("index.shtml", "rewriteSpec2/ is index.shtml");
	});
	casper.run(function() {
		test.done();
	});
});

casper.test.begin('file index.html', 1, function suite(test) {
	casper.start(domain + "/test/rewriteSpec1/index.html", function() {
		test.assertTitle("index.html", "rewriteSpec1/index.html is index.html");
	});
	casper.run(function() {
		test.done();
	});
});


casper.test.begin('file index.shtml', 1, function suite(test) {
	casper.start(domain + "/test/rewriteSpec1/index.shtml", function() {
		test.assertTitle("index.shtml", "rewriteSpec1/index.shtml is index.shtml");
	});
	casper.run(function() {
		test.done();
	});
});


casper.test.begin('file index.php', 1, function suite(test) {
	casper.start(domain + "/test/rewriteSpec1/index.php", function() {
		test.assertTitle("index.php", "rewriteSpec1/index.php is index.php");
	});
	casper.run(function() {
		test.done();
	});
});

casper.test.begin('rewrite index.html to index.shtml', 1, function suite(test) {
	casper.start(domain + "/test/rewriteSpec2/index.html", function() {
		test.assertTitle("index.shtml", "index.html to index.shtml");
	});
	casper.run(function() {
		test.done();
	});
});
