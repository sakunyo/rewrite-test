Test mod_rewrite
===

## Usage
Run Test

	$ casperjs test test/spec.js


![fig01.gif](./raw/master/fig01.gif)

## Tree

	.
	├── README.md # this file
	│
	├── .htaccess
	│
	│ # Test Resources.
	├── rewriteSpec1
	│   ├── index.html
	│   ├── index.php
	│   └── index.shtml
	├── rewriteSpec2
	│   └── index.shtml
	│
	└── test
		└── spec.js # CasperJS spec


